<?php
namespace App; //命名空間

use App\Objects; //使用命名空間中的class
use App\Services;

$objectsPath = __DIR__ . '/app/Objects/*.php'; // 存放object的路徑
$servicesPath = __DIR__ . '/app/Services/*.php'; // 存放service的路徑

foreach (glob($objectsPath) as $object) { //每一筆檔案物件
    require_once $object;
}

foreach (glob($servicesPath) as $service) { //每一筆檔案物件
    require_once $service;
}

require __DIR__ . '/vendor/autoload.php'; //要讀取套件的檔案

function ExcelProcessor($reverse = false)
{
    $obj = new Services\LoadAllExcelService; //創建讀取檔案的物件
    $lotteries = $obj->load($reverse);
    return $lotteries;
}

function getVirtualLottery($num)
{
    $obj = new Services\VirtualLottery($num); // 建立虛擬票卷
    return $obj->get();
}

function initStatistics($num = null)
{
    return $array_statistics = array(
        0 => (isset($num))? $num : 0,
        1 => 0,
        2 => 0,
        3 => 0,
        4 => 0,
        5 => 0,
        6 => 0,
        7 => 0,
        8 => 0,
        9 => 0,
        10 => 0,
        11 => 0,
        12 => 0,
    );
}
ini_set('memory_limit', '-1');
$reverse_mode = false;
echo "please enter a target line count (or r for reverse mode):";
echo "\n:";
$strTarget = trim(fgets(STDIN));
if ($strTarget == 'r') {
    $reverse_mode = true;
    $strTarget = 0;
} elseif (!is_numeric($strTarget)) {
    $strTarget = 12;
} else {
    $strTarget = (int) $strTarget;
}
echo "target line count set to {$strTarget}";
echo "\n";

$lotteries = ExcelProcessor($reverse_mode);
$history = [];
$sortHistory = $history;
$flagAlive = true;
while ($flagAlive) {
    if (!isset($show)) {
        $show = 'NULL';
        $showHis = json_encode($history);
        sort($sortHistory);
        $sortHis = json_encode($sortHistory);
    }
    echo "##################################\n# please enter a number(or exit) #\n# History：{$showHis} \n# Sorting：{$sortHis}\n##################################";
    echo "\n:";
    $str = trim(fgets(STDIN));
    ($str === 'exit') ? $str = 'exit' : true;
    switch ($str) {
        case 'exit':
            $flagAlive = false;
            echo "BYE\n";
            break;
        default:
            if (!is_numeric($str)) {
                $str = '';
            }

            $num = (int) $str;
            $show = $num;
            $history[] = $num;
            $showHis = json_encode($history);
            $sortHistory = $history;
            sort($sortHistory);
            $sortHis = json_encode($sortHistory);
            foreach ($lotteries as $lottery) {
                list($winCount, $chg_flag) = $lottery->hit($num);
                if ($chg_flag && $winCount >= $strTarget) {
                    $info = $lottery->show();
                    echo "\n\n" .
                        "[賓果條數：$winCount], " .
                        "[id：{$info[0]}], [sid：{$info[1]}], " .
                        "[首列號碼：{$info[2][0]}, {$info[2][1]}, {$info[2][2]}, {$info[2][3]}, {$info[2][4]}], " .
                        "[垂直起始點：{$info[3]}, 水平起始點：{$info[4]}, 斜向起始點：{$info[5]}]" .
                        "\n\n";
                }
            }
            break;
    }
}

echo "####################################################\n# please enter a number of lottries for simulation #\n####################################################";
echo "\n:";
$vCount = trim(fgets(STDIN));
if (!is_numeric($vCount)) {
    $vCount = 5000;
}
$array_statistics = initStatistics($vCount);
$notBeenHit = $vCount;
$vLotteries = getVirtualLottery($vCount);
$line11 = $line12 = 0;
$range = range(1, 75);
shuffle($range);
$hitHistory = [];
while (($strTarget != 0 && $array_statistics[$strTarget] == 0) ||
    ($strTarget == 0 && $notBeenHit > 1)) {
    $hitValue = array_splice($range, 0, 1)[0];
    $array_statistics = initStatistics();
    $notBeenHit = 0;
    foreach ($vLotteries as $lottery) {
        list($winCount, $chg_flag) = $lottery->hit($hitValue);
        $lottery_feedback = $lottery->show();
        $bCount = $lottery_feedback[6];
        $array_statistics[$bCount]++;
        if (!$lottery_feedback[7]) {
            $notBeenHit++;
        }
    }
    $hitHistory[] = $hitValue;
}
$totalLotteries = array_sum($array_statistics);
$runCount = count($hitHistory);
$array_statistics_percent = array_map(function ($value) use ($vCount) {
    return round(($value / $vCount * 100), 2);
}, $array_statistics);
$notBeenHitPercent = round((($notBeenHit / $vCount) * 100), 2);
echo "\n" .
    "完全沒被命中彩卷數:{$notBeenHit}（{$notBeenHitPercent}％）\n" .
    "中0條:{$array_statistics[0]}（{$array_statistics_percent[0]}％）\n" .
    "中1條:{$array_statistics[1]}（{$array_statistics_percent[1]}％）\n" .
    "中2條:{$array_statistics[2]}（{$array_statistics_percent[2]}％）\n" .
    "中3條:{$array_statistics[3]}（{$array_statistics_percent[3]}％）\n" .
    "中4條:{$array_statistics[4]}（{$array_statistics_percent[4]}％）\n" .
    "中5條:{$array_statistics[5]}（{$array_statistics_percent[5]}％）\n" .
    "中6條:{$array_statistics[6]}（{$array_statistics_percent[6]}％）\n" .
    "中7條:{$array_statistics[7]}（{$array_statistics_percent[7]}％）\n" .
    "中8條:{$array_statistics[8]}（{$array_statistics_percent[8]}％）\n" .
    "中9條:{$array_statistics[9]}（{$array_statistics_percent[9]}％）\n" .
    "中10條:{$array_statistics[10]}（{$array_statistics_percent[10]}％）\n" .
    "中11條:{$array_statistics[11]}（{$array_statistics_percent[11]}％）\n" .
    "中12條:{$array_statistics[12]}（{$array_statistics_percent[12]}％）\n" .
    "共參與彩卷張數:{$totalLotteries}\n" .
    "共經歷數字數:{$runCount}\n" .
    "\n";
