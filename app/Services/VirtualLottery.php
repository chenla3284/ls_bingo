<?php
namespace App\Services;

use App\Objects;

class VirtualLottery extends DefaultService
{
    private $range;
    private $numOfLottery;
    public function __construct(int $num)
    {
        $this->range = range(1, 75);
        $this->numOfLottery = $num;
    }

    public function get(): array
    {
        $array_return = [];
        $num = $this->numOfLottery;
        for ($i = 0; $i<$num; $i++) {
            $id = $this->randId();
            $sid = $this->randId(true);
            $value = $this->rand25FromRange();
            $value = $this->translateToLotteryArray($value);
            $array_return[] = new Objects\Lottery($id, $sid, $value);
        }
        return $array_return;
    }

    private function rand25FromRange(): array
    {
        $range = $this->range;
        $rndKeys = array_rand($range, 25);
        $rndValues = array_map(function ($key) use ($range) {
            return $range[$key];
        }, $rndKeys);
        shuffle($rndValues);
        return $rndValues;
    }

    private function translateToLotteryArray(array $values): array
    {
        $array_return = [];
        while (!empty($values)) {
            $row = array_splice($values, 0, 5);
            $array_return[] = $row;
        }
        // $p = json_encode($array_return);
        // echo "\n{$p}";
        return $array_return;
    }

    private function randId($subMode = false)
    {
        list($main, $sub) = explode('.', uniqid('', true));
        if ($subMode) {
            return $sub;
        }

        return $main;
    }
}
