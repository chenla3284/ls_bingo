<?php
namespace App\Services;

use App\Objects;

class LoadAllExcelService extends DefaultService
{
    public function load($reverse = false): array
    {
        $arr_return = [];
        $fileList = glob($this->rootPath() . '/inputExcel/*.xlsx'); //將所有檔案列出，存進array
        foreach ($fileList as $file) { //逐一執行每個檔案
            $obj = new Objects\LotteryExcelReader($file, $reverse);
            $arr_return = array_merge($arr_return, $obj->output());
        }
        return $arr_return;
    }
}
