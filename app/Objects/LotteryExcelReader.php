<?php
namespace App\Objects;

use \PhpOffice\PhpSpreadsheet\IOFactory;

class LotteryExcelReader extends DefaultObject
{
    private $id;
    private $filePath;
    private $array_lottery = [];
    private $reverse;
    public function __construct(string $filePath, $reverse = false)
    {
        $this->reader = IOFactory::createReader('Xlsx'); //引用套件，創建讀取'xlsx'的對象
        $this->reader->setReadDataOnly(true); //設置只讀
        $this->filePath = $filePath;
        $this->reverse = $reverse;
        $this->readSingleExcel();
    }

    public function output(): array
    {
        return $this->array_lottery;
    }

    private function readSingleExcel()
    {
        $path = $this->filePath;
        $spreadsheet = $this->reader->load($path); //加載excel文件
        $this->id = basename($path); //讀取純檔名
        $sheetNames = $spreadsheet->getSheetNames();
        foreach ($sheetNames as $sheetName) {
            $worksheet = $spreadsheet->getSheetByName($sheetName); //如果有多個sheet可以循環讀取
            $this->readSingleSheet($sheetName, $worksheet);
        }
    }

    private function readSingleSheet($sid, $worksheet)
    {
        $twoDValue = [];
        $rowMax = $worksheet->getHighestRow(); //總列數，循環用
        for ($row = 1; $row <= $rowMax; $row++) { //for迴圈 從row為1開始，小於等於(<=)最大行數時啟動，每圈row+1
            $twoDValue[] = $this->eachDataRow($worksheet, $row);
        }
        $this->array_lottery[] = new Lottery($this->id, $sid, $twoDValue, $this->reverse);
    }

    private function eachDataRow(&$ws, $rowNum): array
    {
        $value = [];
        $value[] = (int)$ws->getCellByColumnAndRow(1, $rowNum)->getValue();
        $value[] = (int)$ws->getCellByColumnAndRow(2, $rowNum)->getValue();
        $value[] = (int)$ws->getCellByColumnAndRow(3, $rowNum)->getValue();
        $value[] = (int)$ws->getCellByColumnAndRow(4, $rowNum)->getValue();
        $value[] = (int)$ws->getCellByColumnAndRow(5, $rowNum)->getValue();
        return $value;
    }
}
