<?php

namespace App\Objects;

use App\Objects\LotteryExcelUnit;

class Lottery extends DefaultObject
{
    const WIN_FN = 'winCheck';
    const WIN_FN_REVERSE = 'reverseWinCheck';
    private $lottery_data;
    private $win_v_point;
    private $win_h_point;
    private $win_t_point;
    private $previous_bc = 0;
    private $current_bc = 0;
    private $winCheckFunction;
    private $beenHit = false;
    public function __construct($id, $sid, array $value, $reverse_mode = false)
    {
        $this->lottery_data = $this->dataLoad($id, $sid, $value);
        $this->winCheckFunction = ($reverse_mode) ? self::WIN_FN_REVERSE : self::WIN_FN;
    }

    public function hit(int $num)
    {
        $checkFunction = $this->winCheckFunction;
        $array_map = $this->searchValue($num);
        $this->setFlag($array_map, true);
        $array = $this->$checkFunction();
        return $array;
    }

    public function deHit(int $num)
    {
        $checkFunction = $this->winCheckFunction;
        $array_map = $this->searchValue($num);
        $this->setFlag($array_map, false);
        $array = $this->$checkFunction();
        return $array;
    }

    /**
     * 顯示當前狀態
     *
     * @return array
     */
    public function show(): array
    {
        return [
            $this->lottery_data['id'],
            $this->lottery_data['sub_id'],
            $this->lottery_data['value'][0],
            json_encode($this->win_v_point),
            json_encode($this->win_h_point),
            json_encode($this->win_t_point),
            $this->current_bc,
            $this->beenHit,
        ];
    }

    /**
     * 讀取賓果卷資料
     *
     * @return array
     */
    private function dataLoad($id, $sid, array $value): array
    {
        $arr = [
            'id' => $id,
            'sub_id' => $sid,
            'value' => $value,
            'flag' => $this->flagInit()
        ];
        return $arr;
    }

    /**
     * 號碼搜尋
     *
     * @param integer $num
     * @return void
     */
    private function searchValue(int $num): array
    {
        // return 陣列index 或 []
        $value = $this->lottery_data['value'];
        foreach ($value as $key => $row) {
            if (false !== ($p = array_search($num, $row))) {
                return [$key, $p];
            }
        }
        return [];
    }

    /**
     * 確認中獎狀態
     *
     * @return void
     */
    private function winCheck()
    {
        $r_change = false;
        $winLine = 0;
        $vertical = 0;
        $horizontal = 0;
        $tilted = 0;
        $array_vs = $array_hs = $array_ts = [];
        $flag = $this->lottery_data['flag'];
        $value = $this->lottery_data['value'];

        # 檢查垂直
        for ($i = 0; $i <= 4; $i++) {
            if ($flag[0][$i] && $flag[1][$i] && $flag[2][$i] && $flag[3][$i] && $flag[4][$i]) {
                $vertical++;
                if (!array_search($value[0][$i], $array_vs)) {
                    $array_vs[] = $value[0][$i];
                }
            }
        }

        # 檢查水平
        for ($i = 0; $i <= 4; $i++) {
            if ($flag[$i][0] && $flag[$i][1] && $flag[$i][2] && $flag[$i][3] && $flag[$i][4]) {
                $horizontal++;
                if (!array_search($value[$i][0], $array_hs)) {
                    $array_hs[] = $value[$i][0];
                }
            }
        }

        # 檢查右斜
        if ($flag[0][0] && $flag[1][1] && $flag[2][2] && $flag[3][3] && $flag[4][4]) {
            $tilted++;
            if (!array_search($value[0][0], $array_ts)) {
                $array_ts[] = $value[0][0];
            }
        }

        # 檢查左斜
        if ($flag[0][4] && $flag[1][3] && $flag[2][2] && $flag[3][1] && $flag[4][0]) {
            $tilted++;
            if (!array_search($value[0][4], $array_ts)) {
                $array_ts[] = $value[0][4];
            }
        }

        $winLine = $vertical + $horizontal + $tilted;
        $this->previous_bc = $this->current_bc;
        $this->current_bc = $winLine;
        if ($this->previous_bc != $this->current_bc) {
            $r_change = true;
        }

        $this->win_v_point = $array_vs;
        $this->win_h_point = $array_hs;
        $this->win_t_point = $array_ts;
        return [$winLine, $r_change];
    }

    private function reverseWinCheck()
    {
        $flag = $this->lottery_data['flag'];
        if ($flag === $this->flagInit()) {
            return [0, true];
        }

        return [1, false];
    }

    private function flagInit(): array
    {
        return [
            [false, false, false, false, false],
            [false, false, false, false, false],
            [false, false, true, false, false],
            [false, false, false, false, false],
            [false, false, false, false, false],
        ];
    }

    private function setFlag(array $map, $status)
    {
        if (!empty($map)) {
            $y = $map[0];
            $x = $map[1];
            $this->lottery_data['flag'][$y][$x] = $status;
            if ($map !== [2, 2]) {
                $this->beenHit = true;
            }
        }

        return true;
    }
}
